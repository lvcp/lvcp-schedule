#!/bin/bash
set -e
ruby parse_calendar.rb
mkdir .public
cp web/* .public
cp out/* .public
mv .public public