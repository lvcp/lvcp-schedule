require 'rubygems'
require 'bundler/setup'

require 'icalendar'
require 'date'
require 'json'
require 'icalendar-recurrence'

cal_file = File.open('schedule.ics')
cals = Icalendar.parse(cal_file)
cal = cals.first


schedule_strings = Hash.new

cal.events.each { |evt|
    occurrences = evt.occurrences_between(Date.parse('2016-08-22'), Date.parse('2050-12-31'))
    occurrences.each { |occurrence|
        if !schedule_strings.has_key?(occurrence.start_time.to_date.to_s) || evt.recurrence_id then
            schedule_strings[occurrence.start_time.to_date.to_s] = evt.description
        end
    }
}

schedule_strings_to_schedules = Hash.new

schedule_parse_regex = /(\d\d?:\d\d)\s*-\s*(\d\d?:\d\d)[ap]?m?\s*([\w ]*)/

schedule_strings.values.uniq.each { |schedule_string|
    schedule_strings_to_schedules[schedule_string] = schedule_string.scan(schedule_parse_regex).map { |cap| {start: cap[0], end: cap[1], name: cap[2]}}
}

unique_schedules = schedule_strings_to_schedules.values.uniq
days_to_scheduleids = Hash.new

schedule_strings.each_pair { |day, schedule_string|
    days_to_scheduleids[day] = unique_schedules.index(schedule_strings_to_schedules[schedule_string])
}

Dir.mkdir("out") unless File.exists?("out")
File.open("out/schedule.json", "w").write(JSON.generate({days: days_to_scheduleids, schedules: unique_schedules}))
File.open("out/schedule.js", "w").write("window.scheduleData = "+JSON.generate({days: days_to_scheduleids, schedules: unique_schedules}))